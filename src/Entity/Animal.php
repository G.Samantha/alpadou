<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Animal
{
	private ?int $id;
	#[Assert\NotBlank]
	private ?string $name;
	private ?string $img;
	private ?string $content;
	/**
	 * @param int|null $id
	 * @param string|null $name
	 * @param string|null $img
	 * @param string|null $content
	 */
	public function __construct(?string $name, ?string $img, ?string $content, ?int $id = null)
	{
		$this->id = $id;
		$this->name = $name;
		$this->img = $img;
		$this->content = $content;
	}
	/**
	 * @return 
	 */
	public function getId(): ?int
	{
		return $this->id;
	}
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self
	{
		$this->id = $id;
		return $this;
	}
	/**
	 * @return 
	 */
	public function getName(): ?string
	{
		return $this->name;
	}
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getImg(): ?string
	{
		return $this->img;
	}

	/**
	 * @param  $img 
	 * @return self
	 */
	public function setImg(?string $img): self
	{
		$this->img = $img;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getcontent(): ?string
	{
		return $this->content;
	}

	/**
	 * @param  $content 
	 * @return self
	 */
	public function setcontent(?string $content): self
	{
		$this->content = $content;
		return $this;
	}
}