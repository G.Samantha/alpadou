<?php

namespace App\Entity;
use App\Entity\Role;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, PasswordAuthenticatedUserInterface {
private ?int $id;
#[Assert\NotBlank]
private ?string $name;
private ?string $lastname;
private ?string $phone;
#[Assert\Email]
#[Assert\NotBlank]
private ?string $email;
#[Assert\NotBlank]
#[Assert\Length(min: 12)]
private ?string $password;
private ?Role $role;

/**
     * @param int|null $id
     * @param string|null $name
     * @param string|null $lastname
     * @param string|null $phone
     * @param string|null $email
     * @param string|null $password
     * @param array|null $role
     */
	public function __construct(?string $name, ?string $lastname, ?string $phone, ?string $email, ?string $password, ?Role $role,?int $id=null){
		$this->id=$id;
		$this ->name=$name;
		$this ->lastname=$lastname;
		$this ->phone=$phone;
		$this ->email=$email;
		$this ->password=$password;
		$this ->role=$role;
		
		}
	

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getLastname(): ?string {
		return $this->lastname;
	}
	
	/**
	 * @param  $lastname 
	 * @return self
	 */
	public function setLastname(?string $lastname): self {
		$this->lastname = $lastname;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getPhone(): ?string {
		return $this->phone;
	}
	
	/**
	 * @param  $phone 
	 * @return self
	 */
	public function setPhone(?string $phone): self {
		$this->phone = $phone;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEmail(): ?string {
		return $this->email;
	}
	
	/**
	 * @param  $email 
	 * @return self
	 */
	public function setEmail(?string $email): self {
		$this->email = $email;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getPassword(): ?string {
		return $this->password;
	}
	
	/**
	 * @param  $password 
	 * @return self
	 */
	public function setPassword(?string $password): self {
		$this->password = $password;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getRole(): ?Role {
		return $this->role;
	}
	
	/**
	 * @param  $role 
	 * @return self
	 */
	public function setRole(?Role $role): self {
		$this->role = $role;
		return $this;
	}

    public function getUserIdentifier():string {
        return $this->email;
    }
    public function getRoles():array {
        return [$this->role];
    }

    public function eraseCredentials(){}

}