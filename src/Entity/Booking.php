<?php

namespace App\Entity;

use App\Entity\Animal;
use App\Entity\Circuit;
use App\Entity\User;

class Booking
{
    private ?int $id;
    private ?int $score;
    private ?Animal $animal;
    private ?Circuit $circuit;
    private ?User $user;

    public function __construct(?string $score, ?string $animal, ?string $circuit, ?string $user, ?int $id)
    {
        $this->score = $score;
        $this->animal = $animal;
        $this->circuit = $circuit;
        $this->user = $user;
        $this->id = $id;
    }

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getScore(): ?int {
		return $this->score;
	}
	
	/**
	 * @param  $score 
	 * @return self
	 */
	public function setScore(?int $score): self {
		$this->score = $score;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getAnimal(): ?Animal {
		return $this->animal;
	}
	
	/**
	 * @param  $animal 
	 * @return self
	 */
	public function setAnimal(?Animal $animal): self {
		$this->animal = $animal;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCircuit(): ?Circuit {
		return $this->circuit;
	}
	
	/**
	 * @param  $circuit 
	 * @return self
	 */
	public function setCircuit(?Circuit $circuit): self {
		$this->circuit = $circuit;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getUser(): ?User {
		return $this->user;
	}
	
	/**
	 * @param  $user 
	 * @return self
	 */
	public function setUser(?User $user): self {
		$this->user = $user;
		return $this;
	}
}