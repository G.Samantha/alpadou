<?php

namespace App\Entity;
use App\Entity\Circuit;

class Gallery
{
    private ?int $id;
    private ?string $img;
    private ?Circuit $circuit;

    public function __construct(?string $img, ?Circuit $circuit, ?int $id)
    {
        $this->id = $id;
        $this->img = $img;
        $this->circuit = $circuit;
    }

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getImg(): ?string {
		return $this->img;
	}
	
	/**
	 * @param  $img 
	 * @return self
	 */
	public function setImg(?string $img): self {
		$this->img = $img;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCircuit(): ?Circuit {
		return $this->circuit;
	}
	
	/**
	 * @param  $circuit 
	 * @return self
	 */
	public function setCircuit(?Circuit $circuit): self {
		$this->circuit = $circuit;
		return $this;
	}
}