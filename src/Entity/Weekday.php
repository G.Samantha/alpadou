<?php

namespace App\Entity;
use App\Entity\Circuit;

class Weekday {
private ?int $id;
private ?int $dayOfWeek;
private ?Circuit $circuit;

public function __construct(?string $circuit, ?int $dayOfWeek, ?int $id ) {
 $this->id = $id;   
 $this->dayOfWeek = $dayOfWeek;
 $this->circuit = $circuit;
}



	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDayOfWeek(): ?int {
		return $this->dayOfWeek;
	}
	
	/**
	 * @param  $dayOfWeek 
	 * @return self
	 */
	public function setDayOfWeek(?int $dayOfWeek): self {
		$this->dayOfWeek = $dayOfWeek;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCircuit():Circuit {
		return $this->circuit;
	}
	
	/**
	 * @param  $circuit 
	 * @return self
	 */
	public function setCircuit(Circuit $circuit): self {
		$this->circuit = $circuit;
		return $this;
	}
}