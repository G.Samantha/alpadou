<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Circuit {
private ?int $id;
#[Assert\NotBlank]
private ?string $name;
private ?int $maxSpot;
private ?string $content; 


/**
     * @param int|null $id
     * @param string|null $name
     * @param string|null $score
     * @param string|null $img
     * @param string|null $content
     */
public function __construct(?string $name, ?string $maxSpot, ?string $img, ?string $content,?int $id=null){
$this->id=$id;
$this ->name=$name;
$this ->maxSpot=$maxSpot;
$this->img=$img;
$this->content=$content;
}


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getMaxSpot(): ?int {
		return $this->maxSpot;
	}
	
	/**
	 * @param  $maxSpot 
	 * @return self
	 */
	public function setMaxSpot(?int $maxSpot): self {
		$this->maxSpot = $maxSpot;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param  $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
}