-- Active: 1673947756043@@127.0.0.1@3306@alpadou

-- ALTER TABLE user DROP FOREIGN KEY fk_user_role;

-- ALTER TABLE weekday DROP FOREIGN KEY fk_weekday_circuit;

-- ALTER TABLE booking DROP FOREIGN KEY fk_booking_animal;

-- ALTER TABLE booking DROP FOREIGN KEY fk_booking_circuit;

-- ALTER TABLE booking DROP FOREIGN KEY fk_booking_user;

-- ALTER TABLE gallery DROP FOREIGN KEY fk_gallery_circuit;

-- ALTER TABLE animal_circuit DROP FOREIGN KEY fk_animal_circuit_animal;

-- ALTER TABLE
--     animal_circuit DROP FOREIGN KEY fk_animal_circuit_circuit;

DROP TABLE IF EXISTS booking;

DROP TABLE IF EXISTS animal_circuit;

DROP TABLE IF EXISTS gallery;

DROP TABLE IF EXISTS weekday;

DROP TABLE IF EXISTS circuit;

DROP TABLE IF EXISTS animal;

DROP TABLE IF EXISTS user;

DROP TABLE IF EXISTS role;

-- Table des rôles

CREATE TABLE
    role (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        label VARCHAR(255)
    );

-- Table des utilisateurs connecter

CREATE TABLE
    user (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(100) NOT NULL,
        lastname VARCHAR(100),
        phone VARCHAR(100),
        email VARCHAR(255) UNIQUE NOT NULL,
        password VARCHAR(24) NOT NULL,
        id_role INT,
        FOREIGN KEY fk_user_role (id_role) REFERENCES role(id)
    );

-- Table des animaux

CREATE TABLE
    animal (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(255) NOT NULL,
        img VARCHAR(255),
        content TEXT
    );

-- Table des circuits

CREATE TABLE
    circuit (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(255) NOT NULL,
        max_spot INT NOT NULL,
        content TEXT
    );

-- Table des weekdays ()

CREATE TABLE
    weekday (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        day_of_week INTEGER,
        id_circuit INT,
        FOREIGN KEY fk_weekday_circuit (id_circuit) REFERENCES circuit(id)
    );

-- Table des réservations

CREATE TABLE
    booking (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        score TINYINT,
        id_animal INT,
        FOREIGN KEY fk_booking_animal (id_animal) REFERENCES animal(id),
        id_circuit INT,
        FOREIGN KEY fk_booking_circuit (id_circuit) REFERENCES circuit(id),
        id_user INT,
        FOREIGN KEY fk_booking_user (id_user) REFERENCES user(id)
    );

-- Table des galeries

CREATE TABLE
    gallery (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        img VARCHAR(10000),
        id_circuit INT,
        FOREIGN KEY fk_gallery_circuit (id_circuit) REFERENCES circuit(id)
    );

-- Table de liaison entre animaux et circuits

CREATE TABLE
    animal_circuit (
        id_animal INT,
        id_circuit INT,
        PRIMARY KEY (id_animal, id_circuit),
        FOREIGN KEY fk_animal_circuit_animal (id_animal) REFERENCES animal(id),
        FOREIGN KEY fk_animal_circuit_circuit (id_circuit) REFERENCES circuit(id)
    );

INSERT INTO role (label) VALUES('ADMIN');

INSERT INTO role (label) VALUES('USER');

INSERT INTO
    animal (name, img, content)
VALUES (
        'Alpaga',
        'https://images.pexels.com/photos/3396661/pexels-photo-3396661.jpeg',
        'L\'alpaga est un herbivore de la famille des chameaux (camélidés) originaire d\'Amérique du Sud. Il serait une version domestiquée de la vigogne, qui vit sur les hauts plateaux des Andes. Sa laine fournie et d\'excellente qualité a fait sa renommée dans l\'industrie textile'
    ), (
        'Lama',
        'https://images.pexels.com/photos/3727251/pexels-photo-3727251.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'Mammifère ruminant appartenant à la famille des Camélidés, d\assez grande taille, à long cou et hautes pattes, vivant dans les Andes à des altitudes élevées, et dont la toison laineuse sert à fabriquer des tissus très appréciés. L\'alpaga et la vigogne sont des variétés de lamas.'
    ), (
        'Patou',
        'https://images.pexels.com/photos/7440123/pexels-photo-7440123.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'Le chien de montagne des Pyrénées, ou montagne des Pyrénées est une race ancienne de chien de berger, utilisé dans le sud-ouest de la France et le nord-est de l\'Espagne, en particulier les Pyrénées pour la protection des troupeaux contre les préweekdayurs, notamment les ours qui y vivent.'
    );

INSERT INTO
    circuit (name, max_spot, content)
VALUES (
        'Les petits pédestres',
        10,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    ), (
        'En route vers l\aventure',
        8,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    ), (
        'Au sommet nous iront',
        6,
        'Cette balade vous emmenera au sommet des montagnes avec une vue sur le domaine d\Alpadou'
    );

INSERT INTO
    weekday (day_of_week, id_circuit)
VALUES (0, 1), (1, 1), (2, 1), (3, 2), (4, 2), (5, 3), (6, 3);

INSERT INTO
    gallery (img, id_circuit)
VALUES (
        'https://images.pexels.com/photos/554609/pexels-photo-554609.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        1
    ), (
        'https://images.pexels.com/photos/589841/pexels-photo-589841.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        2
    ), (
        'https://images.pexels.com/photos/3680119/pexels-photo-3680119.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        3
    );

INSERT INTO gallery (img)
VALUES (
        'https://images.pexels.com/photos/3680119/pexels-photo-3680119.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'
    );

INSERT INTO
    animal_circuit (id_animal, id_circuit)
VALUES (1, 1), (1, 2), (2, 1), (2, 2), (3, 1), (3, 3);

INSERT INTO
    user (
        name,
        lastname,
        phone,
        email,
        password,
        id_role
    )
VALUES (
        'Joseph',
        'Joestar',
        '',
        'jojo@mail.com',
        '1234',
        2
    ), (
        'Alpadou',
        '',
        '0633464450',
        'alpadou@mail.com',
        'admin',
        1
    );

INSERT INTO
    booking (
        score,
        id_animal,
        id_circuit,
        id_user
    )
VALUES (5, 1, 1, 1), (4, 3, 3, 1);